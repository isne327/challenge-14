#include<iostream>
#include<algorithm>
using namespace std;

void quicksort (int arr[],int start,int end);
void mergesort (int arr[],int size,int start,int end);

int main()
{
	int arr[10] = {10,1,9,2,8,3,7,6,4,5};
	cout << "Before sorting:" << endl;
	for(int i=0 ; i<10 ; i++) 
	{
		cout << arr[i] << " ";
	}
	cout << endl<<endl<<"After Quick Sort:"<<endl;
	
	quicksort(arr,0,9);
	for(int i=0 ; i<10 ; i++) 
	{
		cout << arr[i] << " ";
	}

	
}

void quicksort (int arr[],int start,int end) // Quick sort
{
	int hold_start = start ,hold_end = end , mid = (start+end)/2 +1 , hold[3] ,pivot;
	hold[0] = arr[start];
	hold[1] = arr[mid];
	hold[2] = arr[end];
	
	for(int  i=0; i<2; i++) //Sort the first,last and middle element
	{
		int min = i;
		for(int j=i+1; j < 3 ; j++)
		{
			if(hold[j] < hold[min])
			{
				swap(hold[i],hold[j]);
			}
		}
	}
	
	arr[start] = hold[0];
	arr[mid] = hold[1];
	arr[end] = hold[2];
	pivot = hold[1]; //Set pivot point
	
	while(hold_start <= hold_end) //Sorting by lower element on the left and higher on the right
	{
		while(arr[hold_start] < pivot)
		{
			hold_start++;
		}
		
		while(arr[hold_end] > pivot)
		{
			hold_end--;
		}
			if(hold_start <= hold_end)
			{
				swap(arr[hold_start],arr[hold_end]);
				hold_start++;
				hold_end--;
			}
				
	}
	
	if(start < hold_end) //Sort left side until the size < 2
	{
		quicksort(arr,start,hold_end);
	}
	
	if(end > hold_start) //Sort right side until the size < 2
	{
		quicksort(arr,hold_start,end);
	}
}

void mergesort (int arr[],int size,int start,int end)
{
	int hold[size];
	
}
